<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Domain;

class HomeController extends Controller
{
    public function home(Request $request)
    {

        if(isset($request['search']) && !empty($request['search']))
        {
            $searchValues=$request['search'];
         
            $domain = Domain::where(function ($q) use ($searchValues) {
                $q->orWhere('link_root_domain', 'like', "%{$searchValues}%");
                $q->orWhere('domain_name', 'like', "%{$searchValues}%");
                $q->orWhere('domain_auth', 'like', "%{$searchValues}%");
            })->paginate(10);
        }
        else
        {
            $domain=Domain::paginate(10);

        }
        return view('welcome',compact('domain'));
    }
    public function domainpage($slug)
    {
        if($slug)
        {
            $domain_info=Domain::where('domain_name',$slug)->first();
            if($domain_info)
            {
                return view('domain',compact('domain_info'));
            }
        }
        return redirect('/');


    }
    public function import_domain()
    {

        $file = public_path('csv/top500Domains.csv');
        $domainArr = $this->csvToArray($file);
        //get vendors from data

       // dd($domainArr);

        //sort data
        $sortedData = array();
        foreach($domainArr as $key => $data){

                if($data['Linking Root Domains'] != ''){
                    $linkdomain = str_replace( ',', '', $data['Linking Root Domains'] );
                    $domain= new Domain();
                    $domain->domain_name=$data['Root Domain'];
                    $domain->link_root_domain=$linkdomain;
                    $domain->domain_auth=$data['Domain Authority'];
                    $domain->save();
                }
        }
        dd('done');
    }
    function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $data;
    }
    public function getimage()
    {
        exit;
        ini_set('memory_limit', -1);
        $domain=Domain::where('id','>',234)->get();
        foreach($domain as $value)
        {
            $domain_name=str_replace( '.com', '', $value->domain_name );

            $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://autocomplete.clearbit.com/v1/companies/suggest?query='.$domain_name.'&format=png&size=200',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            ));

            $response = curl_exec($curl);

            curl_close($curl);

            $response=json_decode($response);
            foreach( $response as $domaindata)
            {
                if($domaindata->domain == $value->domain_name)
                {
                    Domain::where('domain_name',$value->domain_name)->update(['domain_image'=>$domaindata->logo]);
                   // dump( $domaindata->domain);
                }
            }
          //  exit;
        }
        dd('done');
    }
}
