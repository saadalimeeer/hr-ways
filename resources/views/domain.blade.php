<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body class="antialiased font-sans bg-gray-200 ">
        <div class="max-w-[85rem] mx-auto px-4 sm:px-6 lg:px-8 py-20 px-20" style="">
            <div class="" style="">

                <div x-data="{ open: false }" @keydown.window.escape="open = false" class="bg-white">
                  <!-- Mobile menu -->

                    <div x-show="open" class="fixed inset-0 flex z-40 lg:hidden" x-description="Off-canvas menu for mobile, show/hide based on off-canvas menu state." x-ref="dialog" aria-modal="true" style="display: none;">

                        <div x-show="open" x-transition:enter="transition-opacity ease-linear duration-300" x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100" x-transition:leave="transition-opacity ease-linear duration-300" x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0" x-description="Off-canvas menu overlay, show/hide based on off-canvas menu state." class="fixed inset-0 bg-black bg-opacity-25" @click="open = false" aria-hidden="true" style="display: none;">
                      </div>


                        <div x-show="open" x-transition:enter="transition ease-in-out duration-300 transform" x-transition:enter-start="-translate-x-full" x-transition:enter-end="translate-x-0" x-transition:leave="transition ease-in-out duration-300 transform" x-transition:leave-start="translate-x-0" x-transition:leave-end="-translate-x-full" x-description="Off-canvas menu, show/hide based on off-canvas menu state." class="relative max-w-xs w-full bg-white shadow-xl pb-12 flex flex-col overflow-y-auto" style="display: none;">


                          <!-- Links -->
                          <div class="mt-2" x-data="Components.tabs()" @tab-click.window="onTabClick" @tab-keydown.window="onTabKeydown">
                            <div class="border-b border-gray-200">
                              <div class="-mb-px flex px-4 space-x-8" aria-orientation="horizontal" role="tablist">

                                  <button id="tabs-1-tab-1" class="flex-1 whitespace-nowrap py-4 px-1 border-b-2 text-base font-medium text-indigo-600 border-indigo-600" x-state:on="Selected" x-state:off="Not Selected" :class="{ 'text-indigo-600 border-indigo-600': selected, 'text-gray-900 border-transparent': !(selected) }" x-data="Components.tab(0)" aria-controls="tabs-1-panel-1" role="tab" x-init="init()" @click="onClick" @keydown="onKeydown" @tab-select.window="onTabSelect" :tabindex="selected ? 0 : -1" :aria-selected="selected ? 'true' : 'false'" type="button" tabindex="0" aria-selected="true">
                                    Women
                                  </button>

                                  <button id="tabs-1-tab-2" class="text-gray-900 border-transparent flex-1 whitespace-nowrap py-4 px-1 border-b-2 text-base font-medium" x-state:on="Selected" x-state:off="Not Selected" :class="{ 'text-indigo-600 border-indigo-600': selected, 'text-gray-900 border-transparent': !(selected) }" x-data="Components.tab(0)" aria-controls="tabs-1-panel-2" role="tab" x-init="init()" @click="onClick" @keydown="onKeydown" @tab-select.window="onTabSelect" :tabindex="selected ? 0 : -1" :aria-selected="selected ? 'true' : 'false'" type="button" tabindex="-1" aria-selected="false">
                                    Men
                                  </button>

                              </div>
                            </div>


                                <div id="tabs-1-panel-1" class="pt-10 pb-8 px-4 space-y-10" x-description="'Women' tab panel, show/hide based on tab state." x-data="Components.tabPanel(0)" aria-labelledby="tabs-1-tab-1" x-init="init()" x-show="selected" @tab-select.window="onTabSelect" role="tabpanel" tabindex="0">
                                  <div class="space-y-4">

                                      <div class="group relative aspect-w-1 aspect-h-1 rounded-md bg-gray-100 overflow-hidden">
                                        <img src="https://tailwindui.com/img/ecommerce-images/mega-menu-category-01.jpg" alt="Models sitting back to back, wearing Basic Tee in black and bone." class="object-center object-cover group-hover:opacity-75">
                                        <div class="flex flex-col justify-end">
                                          <div class="p-4 bg-white bg-opacity-60 text-base sm:text-sm">
                                            <a href="#" class="font-medium text-gray-900">
                                              <span class="absolute inset-0" aria-hidden="true"></span>
                                              New Arrivals
                                            </a>
                                            <p aria-hidden="true" class="mt-0.5 text-gray-700 sm:mt-1">Shop now</p>
                                          </div>
                                        </div>
                                      </div>

                                      <div class="group relative aspect-w-1 aspect-h-1 rounded-md bg-gray-100 overflow-hidden">
                                        <img src="https://tailwindui.com/img/ecommerce-images/mega-menu-category-02.jpg" alt="Close up of Basic Tee fall bundle with off-white, ochre, olive, and black tees." class="object-center object-cover group-hover:opacity-75">
                                        <div class="flex flex-col justify-end">
                                          <div class="p-4 bg-white bg-opacity-60 text-base sm:text-sm">
                                            <a href="#" class="font-medium text-gray-900">
                                              <span class="absolute inset-0" aria-hidden="true"></span>
                                              Basic Tees
                                            </a>
                                            <p aria-hidden="true" class="mt-0.5 text-gray-700 sm:mt-1">Shop now</p>
                                          </div>
                                        </div>
                                      </div>

                                      <div class="group relative aspect-w-1 aspect-h-1 rounded-md bg-gray-100 overflow-hidden">
                                        <img src="https://tailwindui.com/img/ecommerce-images/mega-menu-category-03.jpg" alt="Model wearing minimalist watch with black wristband and white watch face." class="object-center object-cover group-hover:opacity-75">
                                        <div class="flex flex-col justify-end">
                                          <div class="p-4 bg-white bg-opacity-60 text-base sm:text-sm">
                                            <a href="#" class="font-medium text-gray-900">
                                              <span class="absolute inset-0" aria-hidden="true"></span>
                                              Accessories
                                            </a>
                                            <p aria-hidden="true" class="mt-0.5 text-gray-700 sm:mt-1">Shop now</p>
                                          </div>
                                        </div>
                                      </div>

                                  </div>


                                </div>



                          </div>



                        </div>

                    </div>


                  <header class="relative bg-white">
                    <nav aria-label="Top" class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                      <div class="border-b border-gray-200">
                        <div class="h-16 flex items-center justify-between">

                          <!-- Flyout menus -->
                          <div class="hidden lg:flex-1 lg:block lg:self-stretch" x-data="Components.popoverGroup()" x-init="init()">
                            <div class="h-full flex space-x-8">
                                <a href="{{url('/')}}" class="flex items-center text-sm font-medium text-gray-700 hover:text-gray-800">Shop</a>

                            </div>
                          </div>

                          <!-- Logo -->
                          <a href="#" class="flex">
                            <span class="sr-only">Workflow</span>
                            <img class="h-8 w-auto" src="https://tailwindui.com/img/logos/workflow-mark.svg?color=indigo&amp;shade=600" alt="">
                          </a>

                          <div class="flex-1 flex items-center justify-end">

                          </div>
                        </div>
                      </div>
                    </nav>
                </header>

                  <main class="mt-8 max-w-2xl mx-auto pb-16 px-4 sm:pb-24 sm:px-6 lg:max-w-7xl lg:px-8">
                    <div class="lg:grid lg:grid-cols-12 lg:auto-rows-min lg:gap-x-8">
                      <div class="lg:col-start-8 lg:col-span-5">
                        <div class="flex justify-between">
                          <h1 class="text-xl font-medium text-gray-900">
                            {{$domain_info->domain_name}}
                          </h1>
                          <p class="text-xl font-medium text-gray-900">
                            $50
                          </p>
                        </div>
                        <!-- Reviews -->

                      </div>

                      <!-- Image gallery -->
                      <div class="mt-8 lg:mt-0 lg:col-start-1 lg:col-span-7 lg:row-start-1 lg:row-span-3">
                        <h2 class="sr-only">Images</h2>

                        <div class="grid grid-cols-1 lg:grid-cols-2 lg:grid-rows-3 lg:gap-8">

                            @if (empty($domain_info->domain_image))
                            <img src="{{url('placeholder.jfif')}}" alt="{{$domain_info->domain_name}}" class="w-full object-scale-down h-full object-center object-cover lg:w-full lg:h-full">
                            @else
                            <img  src="{{$domain_info->domain_image}}" alt="{{$domain_info->domain_name}}" class="w-full object-scale-down h-full object-center object-cover lg:w-full lg:h-full">
                            @endif

                        </div>
                      </div>

                      <div class="mt-8 lg:col-span-5">
                        <form>
                          <button type="submit" class="mt-8 w-full bg-indigo-600 border border-transparent rounded-md py-3 px-8 flex items-center justify-center text-base font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">Add to cart</button>
                        </form>

                        <!-- Product details -->
                        <div class="mt-10">
                          <h2 class="text-sm font-medium text-gray-900">Link Root Domain</h2>

                          <div class="mt-4 prose prose-sm text-gray-500">

                      <p>{{$domain_info->link_root_domain}}</p>
                          </div>
                        </div>


                        <!-- Policies -->

                      </div>
                    </div>

                    <!-- Reviews -->



                  </main>

                </div>

              </div>
        </div>
    </body>
</html>
