<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body class="antialiased font-sans bg-gray-200 ">
        <div class="max-w-[85rem] mx-auto px-4 sm:px-6 lg:px-8 py-20 px-20" style="">

            <div class="bg-white">

              <div class="max-w-2xl mx-auto py-16 px-4 sm:py-24 sm:px-6 lg:max-w-7xl lg:px-8">
                <h2 class="text-2xl font-extrabold tracking-tight text-gray-900"> Available Domains</h2>
                <form action="{{url('/?search=')}}" method="GET">
                    <div class="pt-6   text-gray-600">
                        <input class="border-2 border-gray-300 bg-white h-10 px-5 pr-16 rounded-lg text-sm focus:outline-none"
                        type="search" name="search" placeholder="Search">
                    </div>
                </form>
                <div class="mt-6 grid grid-cols-1 gap-y-10 gap-x-6 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8">

                    @foreach ($domain as $data)

                        <div class="group relative">
                            <div class="w-full min-h-80 bg-gray-200 aspect-w-1 aspect-h-1 rounded-md overflow-hidden group-hover:opacity-75 lg:h-80 lg:aspect-none">
                                @if (empty($data->domain_image))
                                <img src="{{url('placeholder.jfif')}}" alt="{{$data->domain_name}}" class="w-full object-scale-down h-full object-center object-cover lg:w-full lg:h-full">
                                @else

                                <img src="{{$data->domain_image}}" alt="{{$data->domain_name}}" class="w-full object-scale-down h-full object-center object-cover lg:w-full lg:h-full">
                                @endif

                            </div>
                            <div class="mt-4 flex justify-between">
                                <div>
                                <h3 class="text-sm text-gray-700">
                                    <a href="{{url('/product-detail/'.$data->domain_name)}}">
                                    <span aria-hidden="true" class="absolute inset-0"></span>
                                   {{$data->domain_name}}
                                    </a>
                                </h3>
                                <p class="mt-1 text-sm text-gray-500">Link Root Domain : {{$data->link_root_domain}}</p>
                                </div>
                                <p class="text-sm font-medium text-gray-900">$50</p>
                            </div>
                        </div>
                    @endforeach


                </div>
              </div>
              <nav class="bg-white px-4 py-3 flex items-center justify-between border-t border-gray-200 sm:px-6" aria-label="Pagination">

                <div class="flex-1 flex justify-between sm:justify-end">
                    {{ $domain->withQueryString()->links() }}
                </div>
              </nav>
            </div>

          </div>
    </body>
</html>
